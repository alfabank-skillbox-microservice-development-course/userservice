package rborodin.skillgram.userservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "photos")
@Data
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public class Photo {

    @Id
    private UUID id;

    @Column(name = "link")
    private String link;

    @Column(name = "name")
    private String name;


}

