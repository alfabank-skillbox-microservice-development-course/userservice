package rborodin.skillgram.userservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rborodin.skillgram.userservice.dto.PhotoDTO;
import rborodin.skillgram.userservice.dto.UserDTO;
import rborodin.skillgram.userservice.service.PhotoService;
import rborodin.skillgram.userservice.service.S3PhotoService;
import rborodin.skillgram.userservice.service.UserService;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping(value = "/users")
public class UserController {
    private final UserService userService;
    private final S3PhotoService s3PhotoService;
    private final PhotoService photoService;

    public UserController(UserService userService, S3PhotoService s3PhotoService, PhotoService photoService) {
        this.userService = userService;
        this.s3PhotoService = s3PhotoService;
        this.photoService = photoService;
    }

    @Operation(summary = "Добавление пользователя")
    @PostMapping
    UserDTO createUser(@RequestBody UserDTO userDTO) {
        log.info("Получено событие добавления пользователя");
        return userService.createUser(userDTO);
    }

    @Operation(summary = "Поиск пользователя")
    @GetMapping("/{user_uuid}")
    UserDTO getUser(@PathVariable UUID user_uuid) {
        log.info("Получено событие поиска пользователя по uuid {}", user_uuid);
        return userService.findByUserUuid(user_uuid);
    }

    @Operation(summary = "Обновление пользователя")
    @PutMapping("/{user_uuid}")
    UserDTO updateUser(@RequestBody UserDTO userDTO, @PathVariable UUID user_uuid) {
        log.info("Получено событие поиска пользователя с uuid {}", user_uuid);
        UserDTO userDTOUpdated = userService.updateUser(userDTO, user_uuid);
        userDTOUpdated.setPhotoDTO(userDTO.getPhotoDTO());
        return userDTOUpdated;
    }

    @Operation(summary = "Удаление пользователя")
    @DeleteMapping("/{user_uuid}")
    String deleteUser(@PathVariable UUID user_uuid) {
        log.info("Получено событие удаления пользователя с uuid {}", user_uuid);
        return userService.deleteUser(user_uuid);
    }

    @Operation(summary = "Поиск всех активных пользователей")
    @GetMapping
    List<UserDTO> findAllUsers() {
        log.info("Получено событие поиска всех активных пользователей");
        return userService.findAll().stream()
                .filter(x -> !x.getDeleted())
                .collect(Collectors.toList());
    }

    @Operation(summary = "Добавление фотографии профиля")
    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, path = "/{user_uuid}/photos")
    PhotoDTO uploadPhoto(@PathVariable UUID user_uuid, @RequestPart MultipartFile photo) throws IOException {
        log.info("Получено событие прикрепления фото к пользователю с uuid {}", user_uuid);
        UserDTO userDTO = userService.findByUserUuid(user_uuid);
        log.info("Проверка наличия фото пользователя с uuid {}",user_uuid);
        if(userDTO.getPhotoDTO()!=null){
            log.info("У пользователя уже имеется фото профиля");
            log.info("Удаление предыдущего фото пользователя с uuid {}",user_uuid);
            s3PhotoService.deletePhoto(userDTO.getPhotoDTO().getName());
            photoService.deletePhoto(userDTO.getPhotoDTO().getId());
        }
        log.info("У пользователя не было установлено фото профиля, добавление нового:");
        PhotoDTO photoDTO = s3PhotoService.uploadPhoto(photo);
        return photoService.uploadPhoto(userDTO, photoDTO);
    }

    @Operation(summary = "Поиск фотографии профиля")
    @GetMapping("/{user_uuid}/photos")
    PhotoDTO getPhotoByUserUUID(@PathVariable UUID user_uuid) {
        log.info("Получено поиска фото пользователя с id {}", user_uuid);
        return userService.findByUserUuid(user_uuid).getPhotoDTO();
    }

    @Operation(summary = "Удаление фотографии профиля")
    @DeleteMapping(path = "/{user_uuid}/photos")
    String deletePhoto(@PathVariable UUID user_uuid) {
        log.info("Получено событие удаления фото пользователя с id {}",user_uuid);
        UserDTO userDTO = userService.findByUserUuid(user_uuid);
        s3PhotoService.deletePhoto(userDTO.getPhotoDTO().getName());
        return photoService.deletePhoto(userDTO.getPhotoDTO().getId());
    }

}
