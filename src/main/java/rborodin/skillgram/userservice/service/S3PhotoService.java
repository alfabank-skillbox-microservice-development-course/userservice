package rborodin.skillgram.userservice.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import rborodin.skillgram.userservice.dto.PhotoDTO;
import rborodin.skillgram.userservice.exception.AppException;
import rborodin.skillgram.userservice.propertires.S3Properties;
import rborodin.skillgram.userservice.repository.PhotoRepository;
import rborodin.skillgram.userservice.repository.S3PhotoRepository;

import javax.annotation.Nullable;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class S3PhotoService {

    private final S3PhotoRepository s3PhotoRepository;

    private final S3Properties properties;

    @NewSpan
    public PhotoDTO uploadPhoto(@Nullable MultipartFile photo) throws IOException {
        try {
            assert photo != null;
            try (InputStream stream = new ByteArrayInputStream(photo.getBytes())) {
                log.info("Uploading {} to bucket {} in S3", photo.getOriginalFilename(), properties.getBucketName());
                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentType(MediaType.MULTIPART_FORM_DATA_VALUE);
                metadata.setContentLength(photo.getSize());
                s3PhotoRepository.put(photo.getOriginalFilename(), stream, metadata);
                log.info("Photo '{}' was successfully uploaded", photo.getOriginalFilename());

                PhotoDTO photoDTO = new PhotoDTO();
                photoDTO.setLink(s3PhotoRepository.getURL(photo.getOriginalFilename()).toString());
                log.info("Link:: '{}' ", photoDTO.getLink());

                photoDTO.setName(s3PhotoRepository.get(photo.getOriginalFilename()).get().getKey());
                return photoDTO;
            }
        } catch (AmazonServiceException e) {
            throw new AppException(e.getMessage());
        }
    }

    @NewSpan
    public void deletePhoto(String fileName) {
        String bucketName = properties.getBucketName();
        log.info("Removing {} from bucket {} in S3", fileName, bucketName);
        if (s3PhotoRepository.listKeys(fileName).isEmpty()) {
            log.error("Photo {} have not deleted", fileName);
        }
        s3PhotoRepository.delete(fileName);

        log.info("Photo '{}' was successfully deleted", fileName);
        log.info("Photo {} was successfully deleted", fileName);
    }

    @NewSpan
    public InputStream readPhoto(String key) {
        return s3PhotoRepository.get(key)
                .map(S3Object::getObjectContent)
                .orElseThrow(() -> new IllegalStateException("Object not found " + key));
    }
}
