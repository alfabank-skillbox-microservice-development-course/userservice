package rborodin.skillgram.userservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import rborodin.skillgram.userservice.entity.FollowId;

import java.util.Date;
@Data
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public class FollowDTO {

    private FollowId id;

    private UserDTO followerUser;

    private UserDTO followingUser;


    private Date createdAt;

    public FollowDTO(UserDTO followerUser, UserDTO followingUser) {
        this.id = new FollowId(followerUser.getId(), followingUser.getId());
        this.followerUser = followerUser;
        this.followingUser = followingUser;
    }
}
