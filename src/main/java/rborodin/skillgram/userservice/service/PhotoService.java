package rborodin.skillgram.userservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import rborodin.skillgram.userservice.dto.PhotoDTO;
import rborodin.skillgram.userservice.dto.UserDTO;
import rborodin.skillgram.userservice.entity.Photo;
import rborodin.skillgram.userservice.exception.NotFoundException;
import rborodin.skillgram.userservice.mapper.PhotoMapper;
import rborodin.skillgram.userservice.mapper.UserMapper;
import rborodin.skillgram.userservice.repository.PhotoRepository;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class PhotoService {

    private final PhotoRepository photoRepository;

    @NewSpan
    public PhotoDTO uploadPhoto(UserDTO userDTO, PhotoDTO photoDTO) {
        log.info("Получено событие добавления фотографии профиля");
        Photo photoToAdd = PhotoMapper.INSTANCE.toEntity(photoDTO);
        photoToAdd.setId(userDTO.getId());
        log.info("Пользователь добавлен");
        Photo photo = photoRepository.save(photoToAdd);
        return PhotoMapper.INSTANCE.toDto(photo);
    }

    @NewSpan
    public String deletePhoto(UUID id) {
        if (photoRepository.existsById(id)) {
            photoRepository.deleteById(id);
        } else throw new NotFoundException(String.format("Фото с id %s не найдено в БД", id));
        log.info("Фото с uuid {} успешно удалено", id);
        return String.format("Фото с uuid %s успешно удалено", id);
    }

}
