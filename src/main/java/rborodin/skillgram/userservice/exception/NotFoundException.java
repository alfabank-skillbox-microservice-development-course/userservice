package rborodin.skillgram.userservice.exception;

public class NotFoundException extends AppException {

    public NotFoundException(String message) {
        super(message);
    }

}