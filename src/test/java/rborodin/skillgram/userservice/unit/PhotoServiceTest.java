package rborodin.skillgram.userservice.unit;

import org.instancio.Instancio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import rborodin.skillgram.userservice.dto.PhotoDTO;
import rborodin.skillgram.userservice.dto.UserDTO;
import rborodin.skillgram.userservice.entity.Photo;
import rborodin.skillgram.userservice.exception.NotFoundException;
import rborodin.skillgram.userservice.mapper.PhotoMapper;
import rborodin.skillgram.userservice.repository.PhotoRepository;
import rborodin.skillgram.userservice.repository.UserRepository;
import rborodin.skillgram.userservice.service.PhotoService;
import rborodin.skillgram.userservice.service.UserService;

import javax.persistence.PersistenceException;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

public class PhotoServiceTest {

    static UserRepository userRepository;
    static UserService userService;
    static PhotoRepository photoRepository;
    static PhotoService photoService;

    @BeforeEach
    public void init() {
        userRepository = Mockito.mock(UserRepository.class);
        photoRepository = mock(PhotoRepository.class);
        userService = new UserService(userRepository, photoRepository);
        photoService = new PhotoService(photoRepository);
    }

    @Test
    void uploadPhotoSuccessful() {
        UserDTO userDTO = Instancio.of(UserDTO.class).create();
        PhotoDTO photoDTO = userDTO.getPhotoDTO();
        Mockito.when(photoRepository.save(any(Photo.class))).thenReturn(PhotoMapper.INSTANCE.toEntity(photoDTO));
        PhotoDTO photoCreated = photoService.uploadPhoto(userDTO, photoDTO);
        Assertions.assertEquals(photoDTO, photoCreated);
    }

    @Test
    void uploadPhotoError() {

        //given
        UserDTO userDTO = Instancio.of(UserDTO.class).create();
        PhotoDTO photoDTO = userDTO.getPhotoDTO();
        Mockito.when(photoRepository.save(any(Photo.class))).thenThrow(PersistenceException.class);

        //when
        Executable executable = () -> photoService.uploadPhoto(userDTO,photoDTO);
        //then
        Assertions.assertThrows(PersistenceException.class, executable);
    }


    @Test
    void deletePhotoSuccess(){
        UUID uuid = Instancio.of(UUID.class).create();
        Mockito.when(photoRepository.existsById(uuid)).thenReturn(true);

        //when
        String result = photoService.deletePhoto(uuid);

        //then
        Assertions.assertEquals(String.format("Фото с uuid %s успешно удалено",uuid), result);
    }

    @Test
    void deletePhotoNotFound(){
        UUID uuid = Instancio.of(UUID.class).create();
        Mockito.when(photoRepository.existsById(uuid)).thenReturn(false);

        //when
        Executable executable = () -> photoService.deletePhoto(uuid);

        //then
        Assertions.assertThrows(NotFoundException.class, executable);
    }


}
