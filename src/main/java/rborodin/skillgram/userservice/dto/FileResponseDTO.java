package rborodin.skillgram.userservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;


@Data
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
public class FileResponseDTO {
    String fileName;
    String link;
}
