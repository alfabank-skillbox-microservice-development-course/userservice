package rborodin.skillgram.userservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import rborodin.skillgram.userservice.dto.FollowDTO;
import rborodin.skillgram.userservice.entity.Follow;

import java.util.List;
@Mapper
public interface FollowMapper {

    FollowMapper INSTANCE = Mappers.getMapper(FollowMapper.class);
    FollowDTO toDto(Follow follow);
    Follow toEntity(FollowDTO followDTO);
    List<FollowDTO> toDTOs(List<Follow> follows);

}
