package rborodin.skillgram.userservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import rborodin.skillgram.userservice.dto.FollowDTO;
import rborodin.skillgram.userservice.service.FollowService;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "users/{user_uuid}/follows")
public class FollowController {

    private final FollowService followService;

    public FollowController(FollowService followService) {
        this.followService = followService;
    }

    @PostMapping
    FollowDTO createFollow(@PathVariable UUID user_uuid, @RequestParam UUID followingUserId){
        log.info("Получено событие добавления подписки пользователя с uuid {} на пользователя с uuid {}",user_uuid,followingUserId);
        return followService.createFollow(user_uuid,followingUserId);
    }

    @DeleteMapping
    String deleteFollow(@PathVariable UUID user_uuid,@RequestParam UUID followingUserId){
        return followService.deleteFollow(user_uuid,followingUserId);
    }

    @GetMapping
    List<FollowDTO> findFollows(@PathVariable UUID user_uuid){
        return followService.findAllByUserId(user_uuid);
    }

}
