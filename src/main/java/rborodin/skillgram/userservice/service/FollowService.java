package rborodin.skillgram.userservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import rborodin.skillgram.userservice.dto.FollowDTO;
import rborodin.skillgram.userservice.entity.Follow;
import rborodin.skillgram.userservice.entity.FollowId;
import rborodin.skillgram.userservice.entity.User;
import rborodin.skillgram.userservice.exception.NotFoundException;
import rborodin.skillgram.userservice.mapper.FollowMapper;
import rborodin.skillgram.userservice.mapper.UserMapper;
import rborodin.skillgram.userservice.repository.FollowRepository;
import rborodin.skillgram.userservice.repository.UserRepository;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class FollowService {

    private final FollowRepository followRepository;
    private final UserRepository userRepository;


    public FollowService(FollowRepository followRepository, UserRepository userRepository) {
        this.followRepository = followRepository;
        this.userRepository = userRepository;
    }

    @NewSpan
    public FollowDTO createFollow(UUID followerUserUuid, UUID followingUserUuid) {
        User follower = userRepository.findById(followerUserUuid).orElseThrow(() -> new NotFoundException(String.format("Пользователь с uuid %s отсутствует в БД", followingUserUuid)));
        User following = userRepository.findById(followingUserUuid).orElseThrow(() -> new NotFoundException(String.format("Пользователь с uuid %s отсутствует в БД", followingUserUuid)));
        if (following.getDeleted() == Boolean.TRUE) {
            throw new NotFoundException(String.format("Невозможно подписаться на пользователя с uuid %s, так как страница была удалена",followingUserUuid));
        } else {
            FollowDTO followDTO =new FollowDTO(UserMapper.INSTANCE.toDto(follower), UserMapper.INSTANCE.toDto(following));
            followDTO.setCreatedAt(Date.from(Instant.now()));
            Follow savedFollow = followRepository.save(FollowMapper.INSTANCE.toEntity(followDTO));
            log.info("Подписка успешно добавлена");
            return FollowMapper.INSTANCE.toDto(savedFollow);
        }
    }

    @NewSpan
    public String deleteFollow(UUID followerUserId, UUID followingUserId) {
        if (followRepository.existsById(new FollowId(followerUserId, followingUserId))) {
            followRepository.deleteById(new FollowId(followerUserId, followingUserId));
        } else throw new NotFoundException(String.format("Подписка с uuid %s отсутствует в БД", new FollowId(followerUserId, followingUserId)));
        log.info("Подписка пользователя с uuid {} на пользователя с uuid {} успешно удалена", followerUserId, followingUserId);
        return String.format("Подписка пользователя с id %s на пользователя с id %s удалена", followerUserId, followingUserId);
    }

    @NewSpan
    public List<FollowDTO> findAllByUserId(UUID uuid) {
        if (userRepository.existsById(uuid)) {
            return FollowMapper.INSTANCE.toDTOs(followRepository.findByFollowerUserId(uuid));
        } else throw new NotFoundException(String.format("Пользователь с uuid %s отсутствует в БД", uuid));
    }

}
