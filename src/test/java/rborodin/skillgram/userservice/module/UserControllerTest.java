package rborodin.skillgram.userservice.module;

import io.jaegertracing.testcontainers.JaegerAllInOne;
import io.opentracing.Tracer;
import org.instancio.Instancio;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.testcontainers.containers.MinIOContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import rborodin.skillgram.userservice.dto.PhotoDTO;
import rborodin.skillgram.userservice.dto.UserDTO;
import rborodin.skillgram.userservice.entity.User;
import rborodin.skillgram.userservice.helper.JsonHelper;
import rborodin.skillgram.userservice.repository.PhotoRepository;
import rborodin.skillgram.userservice.repository.UserRepository;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;
import static org.instancio.Select.field;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static rborodin.skillgram.userservice.helper.JsonHelper.fromJson;

/**
 * Класс для модульного тестирования раздела пользователей
 */
@AutoConfigureMockMvc()
@Testcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Container
    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:15");
    @Container
    static MinIOContainer minIOContainer = new MinIOContainer("minio/minio");
    @Container
    static JaegerAllInOne jaegerContainer = new JaegerAllInOne("jaegertracing/all-in-one:1.38");

    @Autowired
    UserRepository userRepository;

    @Autowired
    PhotoRepository photoRepository;

    private static final String SERVICE_NAME = "query-test";
    static Tracer tracer;

    /**
     * Настройка Testcontainer Postgres
     */
    @DynamicPropertySource
    static void configureProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgres::getJdbcUrl);
        registry.add("spring.datasource.username", postgres::getUsername);
        registry.add("spring.datasource.password", postgres::getPassword);
        registry.add("minio.url", minIOContainer::getS3URL);
        registry.add("aws.s3.endpoint", minIOContainer::getS3URL);
        registry.add("aws.s3.access-key", minIOContainer::getUserName);
        registry.add("aws.s3.secret-key", minIOContainer::getPassword);
    }

    @BeforeAll
    static void beforeAll() {
        postgres.start();
        minIOContainer.start();
        jaegerContainer.start();
    }

    /**
     * Создание и инициализация тестовых данных
     */
    @BeforeEach
    void beforeEach() {
        userRepository.deleteAll();
        photoRepository.deleteAll();
        tracer = jaegerContainer.createTracer(SERVICE_NAME);
    }


    @AfterAll
    static void afterAll() {
        postgres.stop();
    }

    @SuppressWarnings("squid:S2699")
    @Test
    void contextLoads() {
    }


    @Test
    @DisplayName("Пользователь успешно создан -> получаем ответ 200; выполнено добавление пользователя в БД")
    void createUserSuccessful() throws Exception {
        UserDTO userDTO = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        String result = mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertEquals(userDTO.getFirstname(), requireNonNull(fromJson(UserDTO.class, result)).getFirstname());

        String resultGet = mockMvc.perform(get("/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertEquals( 1,JsonHelper.parseJsonArray(resultGet, UserDTO.class).size());
    }


    @Test
    void uploadPhoto() throws Exception {

        UserDTO userDTO = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        userDTO = JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));

        var coverPart = new MockMultipartFile("photo", "minio.png", MediaType.IMAGE_PNG_VALUE, Files.readAllBytes(Paths.get("images/minio.png")));

        assert userDTO != null;
        var result = mockMvc.perform(MockMvcRequestBuilders.multipart(String.format("/users/%s/photos", userDTO.getId()))
                        .file(coverPart))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(UTF_8);
        Assertions.assertNotNull(fromJson(PhotoDTO.class, result));

        userDTO = fromJson(UserDTO.class, mockMvc.perform(MockMvcRequestBuilders.get(String.format("/users/%s", userDTO.getId())))
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));

        assert userDTO != null;
        Assertions.assertNotNull(userDTO.getPhotoDTO());

    }

    @Test
    void getPhotoByUserUUIDTest() throws Exception {
        UserDTO userDTO = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        userDTO = JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));

        var coverPart = new MockMultipartFile("photo", "minio.png", MediaType.IMAGE_PNG_VALUE, Files.readAllBytes(Paths.get("images/minio.png")));

        assert userDTO != null;
        PhotoDTO photoDTOCreated = fromJson(PhotoDTO.class, mockMvc.perform(MockMvcRequestBuilders.multipart(String.format("/users/%s/photos", userDTO.getId()))
                        .file(coverPart))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(UTF_8));

        PhotoDTO photoDTO = fromJson(PhotoDTO.class, mockMvc.perform(MockMvcRequestBuilders.get(String.format("/users/%s/photos", userDTO.getId())))
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));

        Assertions.assertEquals(photoDTOCreated, photoDTO);
    }

    @Test
    void updatePhotoSuccess() throws Exception {

        UserDTO userDTO = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        userDTO = JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));

        var coverPart = new MockMultipartFile("photo", "minio.png", MediaType.IMAGE_PNG_VALUE, Files.readAllBytes(Paths.get("images/minio.png")));

        assert userDTO != null;
        var result = mockMvc.perform(MockMvcRequestBuilders.multipart(String.format("/users/%s/photos", userDTO.getId()))
                        .file(coverPart))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(UTF_8);
        Assertions.assertNotNull(fromJson(PhotoDTO.class, result));

        coverPart = new MockMultipartFile("photo", "minio-browser.png", MediaType.IMAGE_PNG_VALUE, Files.readAllBytes(Paths.get("images/minio-browser.png")));
        result = mockMvc.perform(MockMvcRequestBuilders.multipart(String.format("/users/%s/photos", userDTO.getId()))
                        .file(coverPart))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(UTF_8);
        Assertions.assertNotNull(fromJson(PhotoDTO.class, result));

        userDTO = fromJson(UserDTO.class, mockMvc.perform(MockMvcRequestBuilders.get(String.format("/users/%s", userDTO.getId())))
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));

        assert userDTO != null;
        Assertions.assertEquals(userDTO.getPhotoDTO().getName(), "minio-browser.png");
    }

    @Test
    void deletePhotoSuccess() throws Exception {

        UserDTO userDTO = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        userDTO = JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));

        var coverPart = new MockMultipartFile("photo", "minio.png", MediaType.IMAGE_PNG_VALUE, Files.readAllBytes(Paths.get("images/minio.png")));

        assert userDTO != null;
        PhotoDTO resultPhoto = fromJson(PhotoDTO.class, mockMvc.perform(MockMvcRequestBuilders.multipart(String.format("/users/%s/photos", userDTO.getId()))
                        .file(coverPart))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(UTF_8));

        String resultDelete = mockMvc.perform(MockMvcRequestBuilders.delete(String.format("/users/%s/photos", userDTO.getId())))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        assert resultPhoto != null;
        Assertions.assertEquals(String.format("Фото с uuid %s успешно удалено", resultPhoto.getId()), resultDelete);

    }

    @Test
    @DisplayName("Неудачное создание пользователя -> получаем ответ 400; пользователь не добавлен в БД")
    void createUserError() throws Exception {
        mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("error")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is5xxServerError())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);

        String resultGet = mockMvc.perform(get("/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertEquals(JsonHelper.parseJsonArray(resultGet, User.class).size(), 0);
    }

    @Test
    @DisplayName("Пользователь успешно найден по полю ID -> получаем ответ 200")
    void findByIdSuccessful() throws Exception {
        UserDTO userDTO = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        userDTO = JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));
        assert userDTO != null;
        String resultGet = mockMvc.perform(get("/users/" + userDTO.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertEquals(userDTO, fromJson(UserDTO.class, resultGet));
    }

    @Test
    @DisplayName("Пользователь не найден по полю ID -> получаем ответ 404")
    void findByIdNotFound() throws Exception {
        mockMvc.perform(get("/users/" + UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Пользователь не найден по полю ID -> получаем ответ 404; запись не обновлена в БД")
    void updateUserNotFound() throws Exception {
        UserDTO userDTO = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        UserDTO userToUpdate = JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));
        assert userToUpdate != null;
        userToUpdate.setFirstname("Новое_имя");
        mockMvc.perform(put("/users/" + UUID.randomUUID())
                        .content(JsonHelper.toJson(userToUpdate))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        String resultGet = mockMvc.perform(get("/users/" + userToUpdate.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertNotEquals(userDTO, requireNonNull(fromJson(UserDTO.class, resultGet)));

    }

    @Test
    @DisplayName("Пользователь успешно обновлен -> получаем ответ 200; обновлена запись в БД")
    void updateUserSuccessful() throws Exception {
        UserDTO userDTO = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        UserDTO userToUpdate = JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));
        assert userToUpdate != null;
        userToUpdate.setFirstname("Новое_имя");
        String result = mockMvc.perform(put("/users/" + userToUpdate.getId())
                        .content(JsonHelper.toJson(userToUpdate))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertEquals(userToUpdate.getFirstname(), requireNonNull(fromJson(UserDTO.class, result)).getFirstname());

        String resultGet = mockMvc.perform(get("/users/" + userToUpdate.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertEquals("Новое_имя", requireNonNull(fromJson(UserDTO.class, resultGet)).getFirstname());

    }

    @Test
    @DisplayName("Пользователь успешно удален -> получаем ответ 200; выполнено удаление пользователя в БД")
    void deleteUserSuccessful() throws Exception {
        UserDTO userDTO = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        UserDTO userCreated = JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));
        assert userCreated != null;
        String resultDelete = mockMvc.perform(delete("/users/" + userCreated.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertEquals(String.format("Пользователь с uuid %s успешно удален", userCreated.getId()), resultDelete);

        String resultGet = mockMvc.perform(get("/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertEquals(JsonHelper.parseJsonArray(resultGet, UserDTO.class).size(), 0);
    }

    @Test
    @DisplayName("Пользователь не найден по ID -> получаем ответ 200; запись в БД не удалена")
    void deleteUserNotFound() throws Exception {
        UserDTO userDTO = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        UserDTO userToUpdate = JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));
        assert userToUpdate != null;
        mockMvc.perform(delete("/users/" + UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        String resultGet = mockMvc.perform(get("/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertEquals(JsonHelper.parseJsonArray(resultGet, UserDTO.class).size(), 1);
    }

    @Test
    @DisplayName("Все пользователи успешно найдены -> получаем ответ 200")
    void findAllSuccessful() throws Exception {
        UserDTO userDTO1 = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO1))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));
        UserDTO userDTO2 = Instancio.of(UserDTO.class)
                .ignore(field(UserDTO::getId))
                .set(field(UserDTO::getDeleted), false)
                .create();
        JsonHelper.fromJson(UserDTO.class, mockMvc.perform(post("/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonHelper.toJson(userDTO2))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8));
        String result = mockMvc.perform(get("/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8);
        Assertions.assertEquals(JsonHelper.parseJsonArray(result, UserDTO.class).size(), 2);
    }
}