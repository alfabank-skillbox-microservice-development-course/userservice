package rborodin.skillgram.userservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import rborodin.skillgram.userservice.dto.PhotoDTO;
import rborodin.skillgram.userservice.entity.Photo;

import java.util.List;

@Mapper
public interface PhotoMapper {

    PhotoMapper INSTANCE = Mappers.getMapper(PhotoMapper.class);
    PhotoDTO toDto(Photo photo);
    Photo toEntity(PhotoDTO photoDTO);

}
