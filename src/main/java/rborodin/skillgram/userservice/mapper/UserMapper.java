package rborodin.skillgram.userservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import rborodin.skillgram.userservice.entity.User;
import rborodin.skillgram.userservice.dto.UserDTO;

import java.util.List;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
    UserDTO toDto(User user);
    User toEntity(UserDTO userDTO);
    List<UserDTO> toDTOs(List<User> users);
}
