package rborodin.skillgram.userservice.unit;

import org.instancio.Instancio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;
import rborodin.skillgram.userservice.dto.UserDTO;
import rborodin.skillgram.userservice.entity.Photo;
import rborodin.skillgram.userservice.entity.User;
import rborodin.skillgram.userservice.exception.NotFoundException;
import rborodin.skillgram.userservice.mapper.UserMapper;
import rborodin.skillgram.userservice.repository.PhotoRepository;
import rborodin.skillgram.userservice.repository.UserRepository;
import rborodin.skillgram.userservice.service.UserService;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.instancio.Select.field;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class UserServiceTest {

    static UserRepository userRepository;
    static UserService userService;
    static PhotoRepository photoRepository;

    @BeforeEach
    public void init() {
        userRepository = Mockito.mock(UserRepository.class);
        photoRepository = mock(PhotoRepository.class);
        userService = new UserService(userRepository,photoRepository);
    }

    @Test
    void createUserSuccess() {

        //given
        User user = Instancio.of(User.class)
                .ignore(field(User::getId))
                .set(field(User::getDeleted), false)
                .create();
        user.setId(UUID.randomUUID());
        Mockito.when(userRepository.save(any(User.class ))).thenReturn(user);

        //when
        UserDTO updatedUserDTO = userService.createUser(UserMapper.INSTANCE.toDto(user));

        //then
        Assertions.assertEquals(user.getId().toString(), updatedUserDTO.getId().toString());
    }

    @Test
    void createUserError() {

        //given
        User user = Instancio.of(User.class)
                .ignore(field(User::getId))
                .set(field(User::getDeleted), false)
                .create();
        user.setId(UUID.randomUUID());
        Photo photo = Instancio.of(Photo.class).create();
        Mockito.when(userRepository.save(user)).thenThrow(PersistenceException.class);
        Mockito.when(photoRepository.save(photo)).thenReturn(photo);

        //when
        Executable executable = () -> userService.createUser(UserMapper.INSTANCE.toDto(user));
        //then
        Assertions.assertThrows(PersistenceException.class, executable);
    }

    @Test
    void updateUserSuccess() {
        //given
        User user = Instancio.of(User.class)
                .set(field(User::getDeleted), false)
                .create();
        Mockito.when(userRepository.save(user)).thenReturn(user);
        Mockito.when(userRepository.existsById(any(UUID.class))).thenReturn(true);
        Mockito.when(userRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));

        //when
        UserDTO result = userService.updateUser(UserMapper.INSTANCE.toDto(user), UserMapper.INSTANCE.toDto(user).getId());
        //then
        Assertions.assertEquals(user.getId().toString(), result.getId().toString());

    }

    @Test
    void updateUserNotFound() {

        //given
        User user = Instancio.of(User.class)
                .set(field(User::getDeleted), false)
                .create();
        Mockito.when(userRepository.existsById(any(UUID.class))).thenReturn(false);
        //when
        Executable executable = () -> userService.updateUser(UserMapper.INSTANCE.toDto(user), user.getId());

        //then
        Assertions.assertThrows(NotFoundException.class, executable);

    }

    @Test
    void findByIdSuccess() {
        //given
        User user = Instancio.of(User.class)
                .set(field(User::getDeleted), false)
                .create();
        Mockito.when(userRepository.findById(any(UUID.class))).thenReturn(Optional.ofNullable(user));
        //when
        assert user != null;
        String result = String.valueOf(userService.findByUserUuid(user.getId()));

        //then
        Assertions.assertEquals(UserMapper.INSTANCE.toDto(user).toString(), result);
    }

    @Test
    void findByIdNotFound() {
        //given
        User user = Instancio.of(User.class)
                .set(field(User::getDeleted), false)
                .create();
        Mockito.when(userRepository.findById(any(UUID.class))).thenReturn(Optional.empty());
        //when
        Executable executable = () -> userService.findByUserUuid(user.getId());

        //then
        Assertions.assertThrows(NotFoundException.class, executable);
    }

    @Test
    void deleteUserSuccess() {
        //given
        User user = Instancio.of(User.class)
                .set(field(User::getDeleted), false)
                .create();
        Mockito.when(userRepository.save(user)).thenReturn(user);
        Mockito.when(userRepository.existsById(any(UUID.class))).thenReturn(true);
        //when
        String result = userService.deleteUser(user.getId());

        //then
        Assertions.assertEquals(String.format("Пользователь с uuid %s успешно удален", user.getId()), result);
    }

    @Test
    void deleteUserNotFound() {

        //given
        User user = Instancio.of(User.class)
                .set(field(User::getDeleted), false)
                .create();
        Mockito.when(userRepository.existsById(any(UUID.class))).thenReturn(false);
        //when
        Executable executable = () -> userService.deleteUser(user.getId());

        //then
        Assertions.assertThrows(NotFoundException.class, executable);

    }

    @Test
    void findAll() {
        //given
        List<User> users = Instancio.ofList(User.class)
                .set(field(User::getDeleted), false)
                .create();
        Mockito.when(userRepository.findAll()).thenReturn(users);
        //when
        List<UserDTO> result = userService.findAll();
        //then
        Assertions.assertEquals(UserMapper.INSTANCE.toDTOs(users), result);
    }
}