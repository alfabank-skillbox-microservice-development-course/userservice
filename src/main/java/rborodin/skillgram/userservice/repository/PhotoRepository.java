package rborodin.skillgram.userservice.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import rborodin.skillgram.userservice.entity.Photo;

import java.util.UUID;

public interface PhotoRepository extends JpaRepository<Photo, UUID> {
}
