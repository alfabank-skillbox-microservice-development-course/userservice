package rborodin.skillgram.userservice.dto;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;
import java.util.UUID;

@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@RequiredArgsConstructor
public class UserDTO {

    private UUID id;

    private String firstname;

    private String surname;

    private String secondname;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birth;

    private String gender;

    private String email;

    private String phone;

    private Boolean deleted = Boolean.FALSE;

    private PhotoDTO photoDTO;

}
