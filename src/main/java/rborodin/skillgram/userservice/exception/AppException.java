package rborodin.skillgram.userservice.exception;

public class AppException extends RuntimeException {

    public AppException(String message) {
        super(message);
    }
}