package rborodin.skillgram.userservice.repository;

import com.amazonaws.services.s3.AmazonS3;
import org.springframework.stereotype.Component;
import rborodin.skillgram.userservice.propertires.S3Properties;

@Component
public class S3PhotoRepository extends S3Repository {
    public S3PhotoRepository(AmazonS3 s3Client, S3Properties properties) {
        super(s3Client, properties.getBucketName());
    }

}
