package rborodin.skillgram.userservice.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import rborodin.skillgram.userservice.dto.PhotoDTO;
import rborodin.skillgram.userservice.dto.UserDTO;
import rborodin.skillgram.userservice.entity.User;
import rborodin.skillgram.userservice.exception.NotFoundException;
import rborodin.skillgram.userservice.mapper.PhotoMapper;
import rborodin.skillgram.userservice.mapper.UserMapper;
import rborodin.skillgram.userservice.repository.PhotoRepository;
import rborodin.skillgram.userservice.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class UserService {

    private final UserRepository userRepository;
    private final PhotoRepository photoRepository;

    public UserService(UserRepository userRepository, PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
        this.userRepository = userRepository;
    }

    @NewSpan
    public UserDTO updateUser(UserDTO user, UUID userUuid) {
        log.info("Проверка наличия в базе пользователя с uuid {}", userUuid);
        if (userRepository.existsById(userUuid)) {
            log.info("Пользователь с uuid {} успешно найден", userUuid);
            User userToUpdate = userRepository.findById(userUuid).orElseThrow();
            log.info("Начало обновления данных пользователя");
            userToUpdate.setBirth(user.getBirth());
            userToUpdate.setGender(user.getGender());
            userToUpdate.setEmail(user.getEmail());
            userToUpdate.setSurname(user.getSurname());
            userToUpdate.setFirstname(user.getFirstname());
            userToUpdate.setSecondname(user.getSecondname());
            User updatedUser = userRepository.save(userToUpdate);
            log.info("Пользователь с uuid {} успешно обновлен", updatedUser.getId());
            return UserMapper.INSTANCE.toDto(updatedUser);
        } else throw new NotFoundException(String.format("Пользователь с uuid %s отсутствует в БД", userUuid));
    }

    @NewSpan
    public UserDTO createUser(UserDTO userDTO) {
        User savedUser = userRepository.save(UserMapper.INSTANCE.toEntity(userDTO));
        log.info("Пользователь с uuid {} успешно добавлен", savedUser.getId());
        return UserMapper.INSTANCE.toDto(savedUser);
    }

    @NewSpan
    public UserDTO findByUserUuid(UUID userUuid) {
        PhotoDTO photoDTO = PhotoMapper.INSTANCE.toDto(photoRepository.findById(userUuid).orElse(null));
        UserDTO userDTO = UserMapper.INSTANCE.toDto(userRepository.findById(userUuid)
                .orElseThrow(() -> new NotFoundException(String.format("Пользователь с uuid %s отсутствует в БД", userUuid))));
        if (photoDTO != null) {
            userDTO.setPhotoDTO(photoDTO);
        }
        return userDTO;
    }

    @NewSpan
    public String deleteUser(UUID userUuid) {
        log.info("Проверка наличия в базе пользователя с uuid {}", userUuid);
        if (userRepository.existsById(userUuid)) {
            log.info("Удаление пользователя");
            userRepository.deleteById(userUuid);
        } else throw new NotFoundException(String.format("Пользователь с uuid %s отсутствует в БД", userUuid));
        return String.format("Пользователь с uuid %s успешно удален", userUuid);
    }

    @NewSpan
    public List<UserDTO> findAll() {
        return UserMapper.INSTANCE.toDTOs(new ArrayList<>(userRepository.findAll()));
    }
}
