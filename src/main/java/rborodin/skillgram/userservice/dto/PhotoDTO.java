package rborodin.skillgram.userservice.dto;

import lombok.*;

import java.util.UUID;

@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@RequiredArgsConstructor
public class PhotoDTO {
    private UUID id;
    private String link;
    private String name;
}

