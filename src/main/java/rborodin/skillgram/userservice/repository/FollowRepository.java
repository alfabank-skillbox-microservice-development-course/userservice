package rborodin.skillgram.userservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rborodin.skillgram.userservice.entity.Follow;
import rborodin.skillgram.userservice.entity.FollowId;

import java.util.List;
import java.util.UUID;

public interface FollowRepository extends JpaRepository<Follow, FollowId> {


    List<Follow> findByFollowerUserId(UUID uuid);
}
